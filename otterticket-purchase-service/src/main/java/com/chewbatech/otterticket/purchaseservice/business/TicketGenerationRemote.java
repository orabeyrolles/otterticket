package com.chewbatech.otterticket.purchaseservice.business;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class TicketGenerationRemote {
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TicketGenerationRequest {
        private Long ticketId;

        private String artistName;
        private Long concertId;
        private Float price;
        private String category;
        private String zone;
        private Integer row;
        private Integer seatNumber;
        private String concertHallName;
    }

    @Autowired
    private RestTemplate restTemplate;

    public void pushTicketGenerationRequest(TicketGenerationRequest ticketGenerationRequest) {
        restTemplate.postForEntity("http://tickets-generator/ticketgenerationrequest", ticketGenerationRequest, String.class);
    }
}
