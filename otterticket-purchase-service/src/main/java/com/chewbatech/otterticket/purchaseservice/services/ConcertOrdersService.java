package com.chewbatech.otterticket.purchaseservice.services;

import com.chewbatech.otterticket.purchaseservice.business.ConcertRemote;
import com.chewbatech.otterticket.purchaseservice.business.HallRemote;
import com.chewbatech.otterticket.purchaseservice.business.TicketGenerationRemote;
import com.chewbatech.otterticket.purchaseservice.controllers.dto.Order;
import com.chewbatech.otterticket.purchaseservice.domain.PurchasedSeat;
import com.chewbatech.otterticket.purchaseservice.dto.Concert;
import com.chewbatech.otterticket.purchaseservice.dto.ConcertHall;
import com.chewbatech.otterticket.purchaseservice.dto.Seat;
import com.chewbatech.otterticket.purchaseservice.repositories.PurchasedSeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ConcertOrdersService {

    @Autowired
    private PurchasedSeatRepository purchasedSeatRepository;

    @Autowired
    private ConcertRemote concertRemote;
    @Autowired
    private HallRemote hallRemote;
    @Autowired
    private TicketGenerationRemote ticketGenerationRemote;

    @Transactional
    public void pushOrder(Long concertId, List<Order> orders) {

        // Récupération des informations sur le concert / le hall
        final Concert concert = concertRemote.getConcert(concertId);
        final ConcertHall hall = hallRemote.getHall(concert.getConcertHallId());

        for (Order order : orders) {
            final Long orderedSeatId = order.getSeatId();

            // Récupération des données du siège (placement, catégorie, etc...)
            final Seat remoteSeat = hallRemote.getSeat(concert.getConcertHallId(), orderedSeatId);

            // Insertion de l'achat en base
            final PurchasedSeat seat = new PurchasedSeat();
            seat.setConcertId(concertId);
            seat.setSeatId(orderedSeatId);
            final PurchasedSeat save = purchasedSeatRepository.save(seat);

            // Génération du ticket
            final TicketGenerationRemote.TicketGenerationRequest ticketGenerationRequest = new TicketGenerationRemote.TicketGenerationRequest(save.getId(), concert.getArtistName(), concertId, concert.getPrice(), remoteSeat.getCategory().toString(), remoteSeat.getZone(), remoteSeat.getRow(), remoteSeat.getNumber(), hall.getName());
            ticketGenerationRemote.pushTicketGenerationRequest(ticketGenerationRequest);

        }

    }
}
