package com.chewbatech.otterticket.purchaseservice.controllers;

import com.chewbatech.otterticket.purchaseservice.services.ConcertsAvailableSeatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/events/{id}/availableSeats")
public class ConcertsAvailableSeatsController {

    @Autowired
    private ConcertsAvailableSeatsService concertsAvailableSeatsService;

    @GetMapping
    public Long[] getAvailableSeats(@PathVariable("id") Long eventId) {
        return concertsAvailableSeatsService.getAvailableSeatIds(eventId);
    }

    @GetMapping("/count")
    public Integer getAvailableSeatsCount(@PathVariable("id") Long eventId) {
        return concertsAvailableSeatsService.getAvailableSeatIds(eventId).length;
    }
}
