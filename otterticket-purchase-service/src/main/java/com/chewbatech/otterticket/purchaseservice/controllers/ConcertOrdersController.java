package com.chewbatech.otterticket.purchaseservice.controllers;

import com.chewbatech.otterticket.purchaseservice.controllers.dto.Order;
import com.chewbatech.otterticket.purchaseservice.services.ConcertOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/events/{id}/orders")
public class ConcertOrdersController {

    @Autowired
    private ConcertOrdersService concertOrdersService;

    @PostMapping
    public ResponseEntity pushOrder(@PathVariable("id") Long concertId, @RequestBody List<Order> orders) {
        concertOrdersService.pushOrder(concertId, orders);
        return ResponseEntity.noContent().build();
    }
}
