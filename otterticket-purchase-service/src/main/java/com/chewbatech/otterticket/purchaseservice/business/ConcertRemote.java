package com.chewbatech.otterticket.purchaseservice.business;

import com.chewbatech.otterticket.purchaseservice.dto.Concert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ConcertRemote {

    @Autowired
    private RestTemplate restTemplate;

    @Cacheable("remote-concert")
    public Concert getConcert(Long id) {
        return restTemplate.getForEntity("http://concerts-registry/api/concerts/" + id, Concert.class).getBody();
    }

}
