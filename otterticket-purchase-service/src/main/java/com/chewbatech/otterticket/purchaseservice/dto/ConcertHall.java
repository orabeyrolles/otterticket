package com.chewbatech.otterticket.purchaseservice.dto;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
public class ConcertHall {
    private Long id;

    private String name;
    private boolean hasFood;
    private String address;
    private int numberOfSeats;

    private List<Seat> seats;
}
