package com.chewbatech.otterticket.purchaseservice.dto;

import lombok.Data;

import java.util.Date;

@Data
public class Concert {
    private Long id;

    private String artistName;
    private Date date;

    private Long concertHallId;
    private Float price;
}
