package com.chewbatech.otterticket.purchaseservice.business;

import com.chewbatech.otterticket.purchaseservice.dto.ConcertHall;
import com.chewbatech.otterticket.purchaseservice.dto.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HallRemote {

    @Autowired
    private RestTemplate restTemplate;

    @Cacheable("remote-hall")
    public ConcertHall getHall(Long id) {
        return restTemplate.getForEntity("http://concert-halls-registry/api/halls/" + id, ConcertHall.class).getBody();
    }

    @Cacheable("remote-hall-seat")
    public Seat getSeat(Long hallId, Long seatId) {
        return restTemplate.getForEntity("http://concert-halls-registry/api/halls/" + hallId + "/seats/" + seatId, Seat.class).getBody();
    }

}
