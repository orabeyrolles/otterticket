package com.chewbatech.otterticket.purchaseservice.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"hallId", "seatId"})
})
public class PurchasedSeat {
    @Id
    @GeneratedValue
    private Long id;

    private Long concertId;
    private Long hallId;
    private Long seatId;
}
