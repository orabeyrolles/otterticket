package com.chewbatech.otterticket.purchaseservice.controllers.dto;

import lombok.Data;

@Data
public class Order {
    private Long seatId;
}
