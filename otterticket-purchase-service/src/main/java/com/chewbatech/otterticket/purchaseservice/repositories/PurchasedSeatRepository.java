package com.chewbatech.otterticket.purchaseservice.repositories;

import com.chewbatech.otterticket.purchaseservice.domain.PurchasedSeat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchasedSeatRepository extends CrudRepository<PurchasedSeat, Long> {
    List<PurchasedSeat> findByConcertId(Long concertId);
}
