package com.chewbatech.otterticket.purchaseservice.dto;

import lombok.Data;

@Data
public class Seat {

    public enum Category {
        CAT1,
        CAT2,
        VIP,
        FLOOR
    }

    private Long id;

    private Category category;

    private int x;
    private int y;

    private String zone;
    private int row;
    private int number;

}
