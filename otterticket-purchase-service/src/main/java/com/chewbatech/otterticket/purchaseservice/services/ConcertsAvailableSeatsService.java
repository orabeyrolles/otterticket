package com.chewbatech.otterticket.purchaseservice.services;

import com.chewbatech.otterticket.purchaseservice.business.ConcertRemote;
import com.chewbatech.otterticket.purchaseservice.business.HallRemote;
import com.chewbatech.otterticket.purchaseservice.dto.Concert;
import com.chewbatech.otterticket.purchaseservice.dto.Seat;
import com.chewbatech.otterticket.purchaseservice.repositories.PurchasedSeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ConcertsAvailableSeatsService {


    @Autowired
    private ConcertRemote concertRemote;
    @Autowired
    private HallRemote hallRemote;
    @Autowired
    private PurchasedSeatRepository purchasedSeatRepository;

    public Long[] getAvailableSeatIds(Long concertId) {
        // Récupération des places achetées
        Set<Long> purchasedSeatIds = new HashSet<>();
        purchasedSeatRepository.findByConcertId(concertId).stream().forEach(purchasedSeat -> purchasedSeatIds.add(purchasedSeat.getSeatId()));

        // Récupération des places dispo (après récupération du concert pour connaitre la salle)
        Set<Long> seatIds = new HashSet<>();
        final Concert concert = concertRemote.getConcert(concertId);
        final List<Seat> seats = hallRemote.getHall(concert.getConcertHallId()).getSeats();

        // On boucle et on ajoute au résultat les places non achetées
        seats.stream().forEach(seat -> {
            if (!purchasedSeatIds.contains(seat.getId())) {
                seatIds.add(seat.getId());
            }
        });

        Long[] result = new Long[seatIds.size()];
        return seatIds.toArray(result);
    }


}
