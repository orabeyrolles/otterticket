package com.chewbatech.otterticket.concerthallsregistry.repositories;

import com.chewbatech.otterticket.concerthallsregistry.domain.Seat;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatRepository extends PagingAndSortingRepository<Seat, Long> {
}
