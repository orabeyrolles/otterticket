package com.chewbatech.otterticket.concerthallsregistry.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.ACCEPTED)
public class ResourceNotFoundException extends Exception{
}
