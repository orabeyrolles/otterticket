package com.chewbatech.otterticket.concerthallsregistry.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Seat {

    public enum Category {
        CAT1,
        CAT2,
        VIP,
        FLOOR
    }

    @Id
    @GeneratedValue
    private Long id;

    private Category category;

    private int x;
    private int y;

    private String zone;
    private int row;
    private int number;

}
