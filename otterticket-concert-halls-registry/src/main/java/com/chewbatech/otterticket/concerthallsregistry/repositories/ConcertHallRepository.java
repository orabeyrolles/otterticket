package com.chewbatech.otterticket.concerthallsregistry.repositories;

import com.chewbatech.otterticket.concerthallsregistry.domain.ConcertHall;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConcertHallRepository extends PagingAndSortingRepository<ConcertHall, Long> {
}
