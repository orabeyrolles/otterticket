package com.chewbatech.otterticket.concerthallsregistry.controllers;

import com.chewbatech.otterticket.concerthallsregistry.domain.ConcertHall;
import com.chewbatech.otterticket.concerthallsregistry.domain.Seat;
import com.chewbatech.otterticket.concerthallsregistry.exceptions.ResourceNotFoundException;
import com.chewbatech.otterticket.concerthallsregistry.repositories.ConcertHallRepository;
import com.chewbatech.otterticket.concerthallsregistry.repositories.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/halls")
public class ConcertHallController {

    @Autowired
    private ConcertHallRepository concertHallRepository;
    @Autowired
    private SeatRepository seatRepository;

    @GetMapping("/{id}")
    public ConcertHall getHallById(@PathVariable("id") Long hallId) throws ResourceNotFoundException {
        return concertHallRepository.findById(hallId).orElseThrow(ResourceNotFoundException::new);
    }

    @GetMapping
    public Page<ConcertHall> getHalls(Pageable pageable) {
        return concertHallRepository.findAll(pageable);
    }

    @GetMapping("/{hallId}/seats/{seatId}")
    public Seat getSeat(@PathVariable("hallId") Long hallId, @PathVariable("seatId") Long seatId) throws ResourceNotFoundException {
        return seatRepository.findById(seatId).orElseThrow(ResourceNotFoundException::new);
        // TODO il faudrait vérifier que le hallId soit correspondant, mais cela nécessite une liaison bi-directionnelle Seat<->Hall
    }
}
