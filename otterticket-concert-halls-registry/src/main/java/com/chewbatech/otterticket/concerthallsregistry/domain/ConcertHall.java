package com.chewbatech.otterticket.concerthallsregistry.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class ConcertHall {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private boolean hasFood;
    private String address;
    private int numberOfSeats;

    @OneToMany
    private List<Seat> seats;
}
