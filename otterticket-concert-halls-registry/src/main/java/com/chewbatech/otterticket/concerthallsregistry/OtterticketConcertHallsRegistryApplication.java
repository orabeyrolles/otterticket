package com.chewbatech.otterticket.concerthallsregistry;

import com.chewbatech.otterticket.concerthallsregistry.domain.ConcertHall;
import com.chewbatech.otterticket.concerthallsregistry.domain.Seat;
import com.chewbatech.otterticket.concerthallsregistry.repositories.ConcertHallRepository;
import com.chewbatech.otterticket.concerthallsregistry.repositories.SeatRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableEurekaClient
public class OtterticketConcertHallsRegistryApplication {


    public static void main(String[] args) {
        SpringApplication.run(OtterticketConcertHallsRegistryApplication.class, args);
    }


    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        // Do any additional configuration here
        return builder.build();
    }


    public static final int SEAT_HEIGHT = 10;
    public static final int SEAT_WIDTH = 10;

    @Autowired
    private ConcertHallRepository concertHallRepository;
    @Autowired
    private SeatRepository seatRepository;

    @Bean
    InitializingBean sendDatabase() {
        return () -> {
            ConcertHall hall1 = new ConcertHall();
            hall1.setName("Le Dome");
            hall1 = concertHallRepository.save(hall1);
            List<Seat> seats = new ArrayList<>();
            buildSquaredHallSeats(5, 3, 10, 5).forEach((seat) -> seats.add(seatRepository.save(seat)));
            hall1.setSeats(seats);
            hall1.setNumberOfSeats(seats.size());
            hall1.setAddress("Rue des Loutres");
            hall1.setHasFood(true);
            concertHallRepository.save(hall1);
        };
    }

    private List<Seat> buildSquaredHallSeats(int numOfBlocksX, int numOfBlocksY, int numOfSeatsInBlockX, int numOfSeatsInBlockY) {
        List<Seat> seats = new ArrayList<>();
        for (int blockY = 0; blockY < numOfBlocksY; blockY++) {
            for (int blockX = 0; blockX < numOfBlocksX; blockX++) {
                for (int row = 0; row < numOfSeatsInBlockY; row++) {
                    for (int seatNumber = 0; seatNumber < numOfSeatsInBlockX; seatNumber++) {
                        final Seat hallSeat = new Seat();
                        hallSeat.setZone(String.valueOf((char) (blockX * numOfBlocksX + blockY + 65))); // 65 -> Code ASCII du A
                        hallSeat.setRow(row);
                        hallSeat.setNumber(seatNumber);
                        hallSeat.setCategory(buildCategoryForSquaredHallSeat(numOfBlocksX, blockY, blockX));

                        final int groupXOffset = blockX * (numOfSeatsInBlockX + 1); // +1 Pour espacer les blocks
                        hallSeat.setX((seatNumber + groupXOffset) * SEAT_WIDTH);

                        final int groupYOffset = blockY * (numOfSeatsInBlockY + 1); // +1 Pour espacer les blocks
                        hallSeat.setY((row + groupYOffset) * SEAT_HEIGHT);

                        seats.add(hallSeat);
                    }
                }
            }
        }
        return seats;
    }

    private Seat.Category buildCategoryForSquaredHallSeat(int numOfBlocksX, int blockY, int blockX) {
        Seat.Category cat;
        if (blockY == 0) {
            if (blockX > numOfBlocksX * .3 && blockX < numOfBlocksX * .7) {
                cat = Seat.Category.FLOOR;
            } else {
                cat = Seat.Category.VIP;
            }
        } else if (blockY == 1) {
            cat = Seat.Category.CAT1;
        } else {
            cat = Seat.Category.CAT2;
        }
        return cat;
    }
}
