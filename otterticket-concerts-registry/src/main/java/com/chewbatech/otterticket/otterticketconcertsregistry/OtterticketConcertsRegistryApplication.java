package com.chewbatech.otterticket.otterticketconcertsregistry;

import com.chewbatech.otterticket.otterticketconcertsregistry.domain.Concert;

import com.chewbatech.otterticket.otterticketconcertsregistry.repositories.ConcertRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.Instant;
import java.util.Date;

@SpringBootApplication
public class OtterticketConcertsRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtterticketConcertsRegistryApplication.class, args);
    }

    @Autowired
    private ConcertRepository concertRepository;

    @Bean
    InitializingBean sendDatabase() {
        return () -> {
            Concert concert1 = new Concert();
            concert1.setArtistName("Muse");
            concert1.setConcertHallName("Le Dôme");
            concert1.setConcertHallId(1L);
            concert1.setDate(Date.from(Instant.now()));
            concert1.setPrice(10.F);
            concertRepository.save(concert1);

            Concert concert2 = new Concert();
            concert2.setArtistName("John Mayer");
            concert1.setConcertHallName("Le Dôme");
            concert2.setConcertHallId(1L);
            concert2.setDate(Date.from(Instant.now()));
            concert2.setPrice(20.F);
            concertRepository.save(concert2);
        };
    }

}
