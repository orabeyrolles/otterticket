package com.chewbatech.otterticket.otterticketconcertsregistry.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Concert {

    @Id
    @GeneratedValue
    private Long id;

    private String artistName;
    private Date date;

    private Long concertHallId;
    private String concertHallName;
    private Float price;

}
