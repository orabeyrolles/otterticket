package com.chewbatech.otterticket.otterticketconcertsregistry.repositories;

import com.chewbatech.otterticket.otterticketconcertsregistry.domain.Concert;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConcertRepository extends PagingAndSortingRepository<Concert, Long> {
}
