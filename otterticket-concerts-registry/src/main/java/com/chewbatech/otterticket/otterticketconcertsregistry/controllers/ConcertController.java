package com.chewbatech.otterticket.otterticketconcertsregistry.controllers;

import com.chewbatech.otterticket.otterticketconcertsregistry.domain.Concert;
import com.chewbatech.otterticket.otterticketconcertsregistry.exceptions.UnknownResourceException;
import com.chewbatech.otterticket.otterticketconcertsregistry.repositories.ConcertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/concerts")
public class ConcertController {

    @Autowired
    private ConcertRepository concertRepository;

    @GetMapping
    public Page<Concert> getConcertPage(Pageable pageable) {
        return concertRepository.findAll(pageable);
    }

    @GetMapping("/{id}")
    public Concert getConcertPage(@PathVariable("id") Long id) throws UnknownResourceException {
        System.out.println("test");
        return concertRepository.findById(id).orElseThrow(UnknownResourceException::new);
    }


}
