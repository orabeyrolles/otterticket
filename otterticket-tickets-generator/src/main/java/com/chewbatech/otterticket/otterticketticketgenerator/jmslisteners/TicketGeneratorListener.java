package com.chewbatech.otterticket.otterticketticketgenerator.jmslisteners;

import com.chewbatech.otterticket.otterticketticketgenerator.business.PDFGenerator;
import com.chewbatech.otterticket.otterticketticketgenerator.dto.TicketGenerationRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class TicketGeneratorListener {

    @Autowired
    private PDFGenerator pdfGenerator;

    @Value("${TICKET_DST_PATH:/Users/olivieryoda/Desktop/}")
    private String ticketDestinationPath;

    @JmsListener(destination = "ticket-generation-request")
    public void receiveMessage(TicketGenerationRequest ticket) {

        try {
            pdfGenerator.generateAndWritePDF(ticket.toString(), ticketDestinationPath, "Ticket-" + ticket.getTicketId());
        } catch (IOException e) {
            log.error("Impossible de generer le ticket", e);
        }
    }

}
