package com.chewbatech.otterticket.otterticketticketgenerator.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TicketGenerationRequest implements Serializable {

    private Long ticketId;

    private String artistName;
    private Long concertId;
    private Float price;
    private String category;
    private String zone;
    private Integer row;
    private Integer seatNumber;
    private String concertHallName;
}
