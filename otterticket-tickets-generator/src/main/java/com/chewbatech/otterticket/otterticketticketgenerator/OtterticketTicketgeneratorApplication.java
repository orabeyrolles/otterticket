package com.chewbatech.otterticket.otterticketticketgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@SpringBootApplication
@EnableJms
public class OtterticketTicketgeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtterticketTicketgeneratorApplication.class, args);
    }

}
