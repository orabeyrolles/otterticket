package com.chewbatech.otterticket.otterticketticketgenerator.controllers;

import com.chewbatech.otterticket.otterticketticketgenerator.dto.TicketGenerationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.jms.ConnectionFactory;

@RestController
public class TicketGenerationRequestController {

    @Autowired
    private ConnectionFactory connectionFactory;

    private JmsTemplate jmsTemplate;

    @PostConstruct
    public void init() {
        jmsTemplate = new JmsTemplate(connectionFactory);
    }

    @PostMapping("/ticketgenerationrequest")
    public ResponseEntity pushTicketGenerationRequest(@RequestBody TicketGenerationRequest ticketGenerationRequest) {
        jmsTemplate.convertAndSend("ticket-generation-request", ticketGenerationRequest);
        System.out.println(ticketGenerationRequest.getArtistName());
        return ResponseEntity.noContent().build();
    }
}
