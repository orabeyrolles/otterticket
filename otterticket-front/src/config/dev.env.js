var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  env: 'development',
  apiUrl: 'http://localhost:8090/api' // Local seulement
})
