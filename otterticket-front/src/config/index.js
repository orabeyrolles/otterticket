
let isProd = process.env.NODE_ENV === 'production'

export default isProd ? require('./prod.env') : require('./dev.env')
