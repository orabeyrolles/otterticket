import Vue from 'vue'
import Router from 'vue-router'
import ConcertsView from "./views/ConcertsView";
import ConcertReservationView from "./views/ConcertReservationView";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Concerts',
      component: ConcertsView
    },
    {
      path: '/concerts',
      name: 'Concerts',
      component: ConcertsView
    },
    {
      path: '/concerts/:id',
      name: 'ConcertsReservation',
      component: ConcertReservationView
    }
  ]
})
