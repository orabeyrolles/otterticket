import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import axios from 'axios'
import Config from './config'
import VTooltip from 'v-tooltip'
import router from './router'
import moment from 'moment'

Vue.prototype.$http = axios;
Vue.prototype.$config = Config;

Vue.config.productionTip = false

Vue.use(VTooltip)

moment.locale(window.navigator.language)

new Vue({
	router,
	render: h => h(App)
}).$mount('#app')
