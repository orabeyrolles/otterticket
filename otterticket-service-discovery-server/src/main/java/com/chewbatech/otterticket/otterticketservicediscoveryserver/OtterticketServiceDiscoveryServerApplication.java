package com.chewbatech.otterticket.otterticketservicediscoveryserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class OtterticketServiceDiscoveryServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtterticketServiceDiscoveryServerApplication.class, args);
    }

}
